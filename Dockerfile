FROM alpine:latest

RUN mkdir -p /opt/event-alerter
WORKDIR /opt/event-alerter

RUN apk add --update --no-cache bash jq curl

COPY alert.sh .

CMD ["/opt/event-alerter/alert.sh"]
