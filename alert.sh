#!/bin/bash

set -e
set -o pipefail
set -u

CHECK_INTERVAL="${CHECK_INTERVAL:-60}"

SA_TOKEN_FILE="${SA_TOKEN_FILE:-/var/run/secrets/tokens/token}"
CA_CERT_FILE="${CA_CERT_FILE:-/var/run/secrets/tokens/ca.crt}"

SEEN_EVENTS=""

INCLUDE_NAMESPACES="${INCLUDE_NAMESPACES:-}"
EXCLUDE_NAMESPACES="${EXCLUDE_NAMESPACES:-}"

get_events () {
  local token="$(cat "${SA_TOKEN_FILE}")"

  curl \
    --silent \
    -X GET \
    --cacert "${CA_CERT_FILE}" \
    --header "Authorization: Bearer ${token}" \
    "https://${KUBERNETES_PORT_443_TCP_ADDR}/apis/events.k8s.io/v1/events?limit=30"
}

fmt_message () {
  local event="$1"

  local kind="$(echo "${event}" | jq -r ".regarding.kind")"
  local namespace="$(echo "${event}" | jq -r ".regarding.namespace")"
  local name="$(echo "${event}" | jq -r ".regarding.name")"
  local message="$(echo "${event}" | jq -r ".note")"
  local firstTimestamp="$(echo "${event}" | jq -r ".deprecatedFirstTimestamp")"
  local lastTimestamp="$(echo "${event}" | jq -r ".deprecatedLastTimestamp")"

  echo "**${namespace}/${kind}/${name}**\nFirst: ${firstTimestamp}, Last: ${lastTimestamp}\n${message}\n\n"
}

send_alert () {
  local msg="$1"
  echo "$msg"

  payload="$(jq -n --arg content "${msg}" '{username: "alert", content: $content}')"
  printf -v payload_unescape '%b' "${payload}"

  curl \
    -H "Content-Type: application/json" \
    -d "${payload_unescape}" \
    "${WEBHOOK_URL}"
}

check_events () {
  local events="$1"
  local alert_msg=""

  local warnings=0
  for i in $(echo "${events}" | jq -c '.items | keys[]'); do
    event="$(echo "${events}" | jq -r ".items[${i}]")"
    evt_type="$(echo "${event}" | jq -r ".type")"

    if [[ "$evt_type" != "Warning" ]]; then
      continue
    fi

    evt_uid="$(echo "${event}" | jq -r ".metadata.uid")"

    if [[ -n "$(echo "${SEEN_EVENTS}" | grep -F "${evt_uid}")" ]]; then
      continue
    fi

    evt_ns="$(echo "${event}" | jq -r ".metadata.namespace")"
    if [[ -n "${INCLUDE_NAMESPACES}" ]] && [[ -z "$(echo ",${INCLUDE_NAMESPACES}," | tr -d ' ' | grep -F ",${evt_ns},")" ]]; then
      continue
    fi

    if [[ -n "${EXCLUDE_NAMESPACES}" ]] && [[ -n "$(echo ",${EXCLUDE_NAMESPACES}," | tr -d ' ' | grep -F ",${evt_ns},")" ]]; then
      continue
    fi

    alert_msg="${alert_msg}$(fmt_message "${event}")"

    warnings=$((warnings+1))
    SEEN_EVENTS="${SEEN_EVENTS},${evt_uid}"
  done

  if [[ -n "${alert_msg}" ]]; then
    send_alert "${alert_msg}"
  fi
}


send_alert "Started agent."

while true; do
  echo "[$(date -u -Iseconds)] Checking Events"

  events="$(get_events)"
  check_events "${events}"

  sleep "${CHECK_INTERVAL}"
done

